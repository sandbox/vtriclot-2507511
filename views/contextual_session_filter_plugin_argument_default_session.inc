<?php

/**
 * @file
 * Definition of contextual_session_filter_plugin_argument_default_session.
 */

/**
 * Argument handler for arguments that are from session's value.
 *
 * @ingroup views_argument_default_plugins
 */
class contextual_session_filter_plugin_argument_default_session extends views_plugin_argument_default {

  /**
   * Create the option definition.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['session'] = array('default' => '');

    return $options;
  }

  /**
   * Create the options form.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['session'] = array(
      '#type' => 'textfield',
      '#title' => t('Session variable name'),
      '#default_value' => $this->options['session'],
    );
  }

  function convert_options(&$options) {
    if (!isset($options['session']) && isset($this->argument->options['default_argument_session'])) {
      $options['session'] = $this->argument->options['default_argument_session'];
    }
  }

  function get_argument() {
    if (isset($_SESSION[$this->options['session']])) {
      return implode(',', $_SESSION[$this->options['session']]);
    }
  }
}
